# Satellite Telemetry Monitoring

This project is designed to assist satellite ground operations for an Earth science mission. It ingests status telemetry data from satellites and generates alert messages based on specific violation conditions related to battery voltage and thermostat readings.

## Features

- Ingest status telemetry data from an ASCII text file containing pipe-delimited records.
- Generate alert messages for the following violation scenarios:
  - Three battery voltage readings under the red low limit within a five-minute interval for the same satellite.
  - Three thermostat readings that exceed the red high limit within a five-minute interval for the same satellite.
- Output alert messages in JSON format to the console.

## Requirements

- Python>=3.9.x
- Pandas>=1.3.5

## Installation

### Setting up a Virtual Environment (Optional but Recommended)

1. Create a new virtual environment:

   ```
   python3 -m venv myenv
   ```

2. Activate the virtual environment:

   ```
   source myenv/bin/activate
   ```

### Installing Dependencies

To install the required packages, run the following command:

```
pip install -e .
```

## Running the Code

There are two provded options for using the code. One can either call the main file from the command line:

```
python pygeoalert/modular_alert_system.py --input-file examples/input_file.csv
```

Or one can import the functions in a new script. Open python and run the following:

```
from pygeoalert.modular_alert_system import read_data, generate_alerts
import json

df = read_data("examples/input_file.csv")
alerts = generate_alerts(df)
print(json.dumps(alerts, indent=4))
```

## Output

The script will print alert messages in JSON format to the console, detailing any violation conditions detected.

## Unit Tests

To perform unit tests of `pygeoalert/modular_alert_system.py`, simply run:

```
python tests/test_system.py
```

