import pandas as pd
import json
import argparse

def read_data(file_path):
    """
    Reads the file into a Pandas DataFrame and preprocesses it.

    Args:
        file_path (str): Path to the input file.
    
    Returns:
        DataFrame: Preprocessed DataFrame containing satellite telemetry data.
    """
    col_names = ['timestamp', 'satellite_id', 'red_high_limit', 'yellow_high_limit', 'yellow_low_limit', 'red_low_limit', 'raw_value', 'component']
    df = pd.read_csv(file_path, delimiter='|', names=col_names)
    df['timestamp'] = pd.to_datetime(df['timestamp'], format='%Y%m%d %H:%M:%S.%f')
    return df

def check_tstat_conditions(group):
    """
    Checks for violation conditions for TSTAT component within a grouped DataFrame.

    Args:
        group (DataFrame): DataFrame containing satellite telemetry data for TSTAT.
    
    Returns:
        list: List of alert dictionaries for TSTAT violations.
    """
    alerts = []
    condition = group['raw_value'] > group['red_high_limit']
    for i, (index, row) in enumerate(group.iterrows()):
        last_5_min_data = group[(group['timestamp'] >= row['timestamp'] - pd.Timedelta(minutes=5)) & (group['timestamp'] <= row['timestamp'])]
        if condition.loc[last_5_min_data.index].sum() >= 3:
            alerts.append({"satelliteId": int(row['satellite_id']), "severity": "RED HIGH", "component": "TSTAT", "timestamp": row['timestamp'].isoformat() + "Z"})
    return alerts

def check_batt_conditions(group):
    """
    Checks for violation conditions for BATT component within a grouped DataFrame.

    Args:
        group (DataFrame): DataFrame containing satellite telemetry data for BATT.
    
    Returns:
        list: List of alert dictionaries for BATT violations.
    """
    alerts = []
    condition = group['raw_value'] < group['red_low_limit']
    for i, (index, row) in enumerate(group.iterrows()):
        last_5_min_data = group[(group['timestamp'] >= row['timestamp'] - pd.Timedelta(minutes=5)) & (group['timestamp'] <= row['timestamp'])]
        if condition.loc[last_5_min_data.index].sum() >= 3:
            alerts.append({"satelliteId": int(row['satellite_id']), "severity": "RED LOW", "component": "BATT", "timestamp": row['timestamp'].isoformat() + "Z"})
    return alerts

def generate_alerts(df):
    """
    Generates alert messages based on violation conditions.

    Args:
        df (DataFrame): DataFrame containing satellite telemetry data.
    
    Returns:
        list: List of alert dictionaries for violations.
    """
    alerts = []
    for (sat_id, component), group in df.groupby(['satellite_id', 'component']):
        group = group.sort_values('timestamp')
        if component == 'TSTAT':
            alerts += check_tstat_conditions(group)
        elif component == 'BATT':
            alerts += check_batt_conditions(group)
    return alerts

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-file', type=str, required=True, help='Path to the input file.')

    args = parser.parse_args()
    df = read_data(args.input_file)
    alerts = generate_alerts(df)
    print(json.dumps(alerts, indent=4))

