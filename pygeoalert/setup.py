from setuptools import setup, find_packages

setup(
    name='pygeoalert',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'pandas==2.1.1'
    ],
    author='Alex Fitts',
    author_email='fitts.aalex@gmail.com',
    description='Satellite telemetry monitoring for Earth science missions',
)

