import unittest
import pandas as pd
from pygeoalert.modular_alert_system import check_tstat_conditions, check_batt_conditions

class TestAlertGeneration(unittest.TestCase):

    def test_check_tstat_conditions(self):
        data = {'timestamp': pd.to_datetime(['2021-10-01 12:00:00', '2021-10-01 12:01:00', '2021-10-01 12:02:00']),
                'satellite_id': [1000, 1000, 1000],
                'red_high_limit': [100, 100, 100],
                'raw_value': [101, 102, 103],
                'component': ['TSTAT', 'TSTAT', 'TSTAT']}
        df = pd.DataFrame(data)
        alerts = check_tstat_conditions(df)
        self.assertEqual(len(alerts), 1)
        self.assertEqual(alerts[0]['severity'], 'RED HIGH')
        
    def test_check_batt_conditions(self):
        data = {'timestamp': pd.to_datetime(['2021-10-01 12:00:00', '2021-10-01 12:01:00', '2021-10-01 12:02:00']),
                'satellite_id': [1000, 1000, 1000],
                'red_low_limit': [10, 10, 10],
                'raw_value': [9, 8, 7],
                'component': ['BATT', 'BATT', 'BATT']}
        df = pd.DataFrame(data)
        alerts = check_batt_conditions(df)
        self.assertEqual(len(alerts), 1)
        self.assertEqual(alerts[0]['severity'], 'RED LOW')

if __name__ == '__main__':
    unittest.main()

